﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PWSystem.BLL.Contracts.Dtos;

namespace PWSystem.BLL.Contracts.Contracts {
    public interface ITransactionService {
        Task<ResponseDto<TransactionDto>> GetAllAsync(Guid userId, int limit = 20, int offset = 0);
        Task<TransactionDto> GetByIdAsync(Guid id);
        Task AddByUserName(string userFio, decimal amount, Guid currentUserId);
        Task AddAsync(TransactionDto transaction);
        Task RemoveAsync(Guid id);
    }
}