﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PWSystem.BLL.Contracts.Dtos;

namespace PWSystem.BLL.Contracts.Contracts {
    public interface IUserService {
        Task<ICollection<UserDto>> GetAllAsync(int limit = 20, int offset = 0);
        Task<ICollection<UserDto>> GetByRecipientAsync(string recipient, Guid currentUserId);
        Task<UserDto> GetByIdAsync(Guid id);
    }
}