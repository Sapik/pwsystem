﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWSystem.BLL.Contracts.Dtos {
    public class TransactionDto {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime ExecutionDate { get; set; }
        public Guid FromUserId { get; set; }
        public Guid ToUserId { get; set; }

        public UserDto FromUser { get; set; }
        public UserDto ToUser { get; set; }
    }
}
