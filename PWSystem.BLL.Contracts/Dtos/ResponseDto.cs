﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWSystem.BLL.Contracts.Dtos {
    public class ResponseDto<T> {
        public ICollection<T> Result { get; set; }
        public int TotalCount { get; set; }
        public int CurrentCount { get; set; }
        public int Offset { get; set; }

        public ResponseDto(ICollection<T> result, int total, int offset) {
            Result = result;
            TotalCount = total;
            CurrentCount = result.Count;
            Offset = offset;
        }
    }
}
