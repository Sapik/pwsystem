﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;
using PWSystem.App_Start;

[assembly: OwinStartup(typeof(PWSystem.Startup))]
namespace PWSystem {
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
            AutofacConfig.RegisterComponents(app);
        }
    }
}