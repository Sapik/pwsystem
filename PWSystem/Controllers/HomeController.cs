﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PWSystem.BLL.Contracts.Contracts;
using PWSystem.BLL.Contracts.Dtos;
using PWSystem.BLL.Exceptions;
using PWSystem.Models.ViewModels;

namespace PWSystem.Controllers {
    [Authorize]
    public class HomeController : Controller {

        private readonly ITransactionService _transactionService;
        private readonly IUserService _userService;

        public HomeController(ITransactionService transactionService, IUserService userService) {
            _transactionService = transactionService;
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult> Index() {
            ViewBag.Response = await GetTransactions();
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> AutocompleteSearch(string search) {
            var users = await _userService.GetByRecipientAsync(search, Guid.Parse(User.Identity.GetUserId()));
            return Json(users.Select(u => u.FirstName + " " + u.LastName), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> CreateTransaction(TransactionViewModel transaction) {
            if (!ModelState.IsValid) {
                return await FillingIndexView(transaction);
            }
            var transactionDto = Mapper.Map<TransactionDto>(transaction);
            transactionDto.FromUserId = Guid.Parse(User.Identity.GetUserId());
            transactionDto.ToUserId = (await _userService.GetByRecipientAsync(transaction.Recipient, Guid.Parse(User.Identity.GetUserId()))).SingleOrDefault()?.Id ?? Guid.Empty;
            try {
                await _transactionService.AddAsync(transactionDto);
                return RedirectToAction("Index");
            }
            catch (Exception e) {
                AddErrorToModel(e);
                return await FillingIndexView(transaction);
            }
        }

        private async Task<ActionResult> FillingIndexView(TransactionViewModel transaction) {
            ViewBag.Response = await GetTransactions();
            return View("Index", transaction);
        }

        private async Task<ResponseViewModel<TransactionViewModel>> GetTransactions() {
            var transaction = await _transactionService.GetAllAsync(Guid.Parse(User.Identity.GetUserId()));
            return Mapper.Map<ResponseViewModel<TransactionViewModel>>(transaction);
        }

        private void AddErrorToModel(Exception ex) {
            if (ex is ArgumentException ||
                ex is NotFoundException ||
                ex is InsufficientFundsException) {
                ViewBag.ErrorMessage = ex.Message;
            }
            else {
                ViewBag.ErrorMessage = "An unhandled error occurred.";
            }
        }
    }
}