﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWSystem.Models.ViewModels {
    public class ResponseViewModel<T> {
        public ICollection<T> Result { get; set; }
        public int TotalCount { get; set; }
        public int CurrentCount { get; set; }
        public int Offset { get; set; }
    }
}