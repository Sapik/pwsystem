﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWSystem.Models.ViewModels {
    public class UserViewModel {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Balance { get; set; }
    }
}