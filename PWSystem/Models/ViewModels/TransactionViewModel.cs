﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PWSystem.Models.ViewModels {
    public class TransactionViewModel {
        [Required(ErrorMessage="Please enter Amount.")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }
        public DateTime ExecutionDate { get; set; }
        [Required(ErrorMessage="Please enter Recipient.")]
        public string Recipient { get; set; }
    }
}