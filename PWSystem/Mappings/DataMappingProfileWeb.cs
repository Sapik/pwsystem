﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using PWSystem.BLL.Contracts.Dtos;
using PWSystem.Models.ViewModels;

namespace PWSystem.Mappings {
    public class DataMappingProfileWeb : Profile {
        public DataMappingProfileWeb() {
            CreateMap<TransactionDto, TransactionViewModel>()
                .ForMember(t => t.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(t => t.ExecutionDate, opt => opt.MapFrom(src => src.ExecutionDate))
                .ForMember(t => t.Recipient, opt => opt.MapFrom(src => src.ToUser.FirstName + " " + src.ToUser.LastName))
                .ReverseMap()
                .ForMember(t => t.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(t => t.ExecutionDate, opt => opt.MapFrom(src => src.ExecutionDate))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<UserDto, UserViewModel>()
                .ForMember(u => u.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(u => u.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(u => u.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(u => u.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(u => u.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(u => u.Balance, opt => opt.MapFrom(src => src.Balance))
                .ReverseMap()
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<ResponseDto<TransactionDto>, ResponseViewModel<TransactionViewModel>>()
                .ForMember(r => r.Result, opt => opt.MapFrom(src => src.Result))
                .ForMember(r => r.TotalCount, opt => opt.MapFrom(src => src.TotalCount))
                .ForMember(r => r.CurrentCount, opt => opt.MapFrom(src => src.CurrentCount))
                .ForMember(r => r.Offset, opt => opt.MapFrom(src => src.Offset))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}