﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using PWSystem.BLL.Mappings;
using PWSystem.Mappings;

namespace PWSystem {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            UpdateDatabase();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Mapper.Initialize(cfg => {
                cfg.AddProfile<DataMappingProfile>();
                cfg.AddProfile<DataMappingProfileWeb>();
            });
        }

        private void UpdateDatabase() {
            var migrationConfig = new DAL.Migrations.Configuration();
            var migrator = new DbMigrator(migrationConfig);
            migrator.Update();
        }
    }
}
