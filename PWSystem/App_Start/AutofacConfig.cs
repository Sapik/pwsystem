﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Owin;
using PWSystem.BLL.Identity;
using PWSystem.BLL.Services;
using PWSystem.DAL;
using PWSystem.DAL.Contracts;
using PWSystem.DAL.Repositories;

namespace PWSystem.App_Start {
    public class AutofacConfig {
        private static IContainer _container;

        public static void RegisterComponents(IAppBuilder app) {
            var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac
            builder.RegisterType<PWDbContext>().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterType<PWSystemUserStore>().As<PWSystemUserStore>();
            builder.RegisterType<PWSystemUserManager>().As<PWSystemUserManager>();

            builder.RegisterGeneric(typeof(EntityRepository<>)).As(typeof(IBaseRepository<>));
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(TransactionService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();


            //builder.RegisterModule<AutomapperConfig>();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Register the Autofac middleware FIRST, then the Autofac MVC middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();
        }
    }
}