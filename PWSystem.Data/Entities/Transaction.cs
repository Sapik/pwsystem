﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWSystem.Data.Entities.Base;

namespace PWSystem.Data.Entities {
    public class Transaction : BaseEntity {
        public Guid FromUserId { get; set; }
        public Guid ToUserId { get; set; }
        public decimal Amount { get; set; }
        public DateTime ExecutionDate { get; set; }

        public User FromUser { get; set; }
        public User ToUser { get; set; }
    }
}
