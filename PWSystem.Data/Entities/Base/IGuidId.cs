﻿using System;

namespace PWSystem.Data.Entities.Base {
    public interface IGuidId {
        Guid Id { get; set; }
    }
}