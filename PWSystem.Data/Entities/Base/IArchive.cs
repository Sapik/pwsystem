﻿namespace PWSystem.Data.Entities.Base {
    public interface IArchive {
        bool IsArchive { get; set; }
    }
}