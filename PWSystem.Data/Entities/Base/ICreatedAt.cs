﻿using System;
using System.Data;

namespace PWSystem.Data.Entities.Base {
    public interface ICreatedAt {
        DateTime CreatedAt { get; set; }
    }
}