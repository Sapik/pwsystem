﻿using System;

namespace PWSystem.Data.Entities.Base {
    public interface IModifiedAt {
        DateTime ModifiedAt { get; set; }
    }
}