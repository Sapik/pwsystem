﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PWSystem.Data.Entities;

namespace PWSystem.Data.Identity {
    public class AspnetUserRole : IdentityUserRole<Guid> {
        public virtual Role Role { get; set; }
    }
}
