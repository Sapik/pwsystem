﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PWSystem.BLL.Contracts.Dtos;
using PWSystem.Data.Entities;

namespace PWSystem.BLL.Mappings {
    public class DataMappingProfile : Profile {
        public DataMappingProfile() {
            CreateMap<Transaction, TransactionDto>()
                .ForMember(t => t.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(t => t.ExecutionDate, opt => opt.MapFrom(src => src.ExecutionDate))
                .ForMember(t => t.FromUserId, opt => opt.MapFrom(src => src.FromUserId))
                .ForMember(t => t.ToUserId, opt => opt.MapFrom(src => src.ToUserId))
                .ForMember(t => t.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(t => t.FromUser, opt => opt.MapFrom(src => src.FromUser))
                .ForMember(t => t.ToUser, opt => opt.MapFrom(src => src.ToUser))
                .ReverseMap()
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<User, UserDto>()
                .ForMember(u => u.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(u => u.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(u => u.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(u => u.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(u => u.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(u => u.Balance, opt => opt.MapFrom(src => src.Balance))
                .ReverseMap()
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
