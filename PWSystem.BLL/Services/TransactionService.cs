﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PWSystem.BLL.Contracts.Contracts;
using PWSystem.BLL.Contracts.Dtos;
using PWSystem.BLL.Exceptions;
using PWSystem.Data.Entities;
using PWSystem.DAL.Contracts;

namespace PWSystem.BLL.Services {
    public class TransactionService : BaseService, ITransactionService {
        public TransactionService(IUnitOfWork uow) : base(uow) {
        }

        public async Task<ResponseDto<TransactionDto>> GetAllAsync(Guid userId, int limit = 20, int offset = 0) {
            var transactions = await UnitOfWork.GetRepository<Transaction>().AllIncluding(t => t.ToUser).Where(t => !t.IsArchive && t.FromUserId == userId)
                .OrderBy(t => t.Id).Skip(offset).Take(limit).ToListAsync();
            var totalCount = await UnitOfWork.GetRepository<Transaction>().Where(t => !t.IsArchive && t.FromUserId == userId).CountAsync();
            return new ResponseDto<TransactionDto>(Mapper.Map<ICollection<TransactionDto>>(transactions), totalCount, offset);
        }

        public async Task<TransactionDto> GetByIdAsync(Guid id) {
            if (id == Guid.Empty) {
                throw new ArgumentNullException("User id can't be empty.");
            }

            var result = await UnitOfWork.GetRepository<Transaction>()
                .FirstOrDefaultAsync(t => !t.IsArchive && t.Id == id);
            if (result == null) {
                throw new NotFoundException($"Transaction with id = {id} not found.");
            }

            return Mapper.Map<TransactionDto>(result);
        }

        public async Task AddByUserName(string userFio, decimal amount, Guid currentUserId) {
            if (string.IsNullOrEmpty(userFio)) {
                throw new ArgumentNullException("userFio can't be empty.");
            }

            if (amount <= 0) {
                throw new ArgumentException("Amount can't be <= 0.");
            }

            var user = await UnitOfWork.GetRepository<User>()
                .FirstOrDefaultAsync(u => (u.FirstName + " " + u.LastName) == userFio);
            if (user == null) {
                throw new NotFoundException($"User with userFio = {userFio} not found.");
            }

            var transaction = new Transaction() {
                FromUserId = currentUserId,
                ToUserId = user.Id,
                Amount = amount,
                ExecutionDate = DateTime.UtcNow
            };

            UnitOfWork.GetRepository<Transaction>().Add(transaction);
            await UnitOfWork.CommitAsync();
        }

        public async Task AddAsync(TransactionDto transaction) {
            if (transaction == null) {
                throw new ArgumentNullException($"Transaction can't be empty.");
            }

            if (transaction.Amount <= 0) {
                throw new ArgumentException("Amount of transaction can't be could not be <= 0.");
            }

            if (transaction.FromUserId == transaction.ToUserId) {
                throw new ArgumentException($"User sender cannot be user recipient.");
            }

            await UpdateUserAmount(transaction.FromUserId, -transaction.Amount);
            await UpdateUserAmount(transaction.ToUserId, transaction.Amount);

            var entity = Mapper.Map<Transaction>(transaction);
            entity.ExecutionDate = DateTime.UtcNow;
            UnitOfWork.GetRepository<Transaction>().Add(entity);
            await UnitOfWork.CommitAsync();
        }

        private async Task UpdateUserAmount(Guid userId, decimal amount) {
            if (userId == null) {
                throw new NotFoundException($"User id = {userId} can't be null.");
            }

            var user = await UnitOfWork.GetRepository<User>().FindByIdAsync(userId);
            if (user == null) {
                throw new NotFoundException($"User with id = {userId} not found.");
            }

            if (amount + user.Balance < 0) {
                throw new InsufficientFundsException("Transaction sum > then balance.");
            }

            user.Balance += amount;
            UnitOfWork.GetRepository<User>().Update(user);
            await UnitOfWork.CommitAsync();
        }

        public async Task RemoveAsync(Guid id) {
            if (id == Guid.Empty) {
                throw new ArgumentNullException("User id can't be empty.");
            }

            var entity = await UnitOfWork.GetRepository<Transaction>().FindByIdAsync(id);
            if (entity == null) {
                throw new NotFoundException($"Transaction with id = {id} not found.");
            }

            entity.IsArchive = true;
            UnitOfWork.GetRepository<Transaction>().Update(entity);
            await UnitOfWork.CommitAsync();
        }
    }
}
