﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWSystem.DAL.Contracts;

namespace PWSystem.BLL.Services {
    public class BaseService {
        protected readonly IUnitOfWork UnitOfWork;

        public BaseService(IUnitOfWork uow) {
            UnitOfWork = uow;
        }
    }
}
