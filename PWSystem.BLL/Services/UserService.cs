﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PWSystem.BLL.Contracts.Contracts;
using PWSystem.BLL.Contracts.Dtos;
using PWSystem.BLL.Exceptions;
using PWSystem.Data.Entities;
using PWSystem.DAL.Contracts;

namespace PWSystem.BLL.Services {
    public class UserService : BaseService, IUserService {
        public UserService(IUnitOfWork uow) : base(uow) {
        }

        public async Task<ICollection<UserDto>> GetAllAsync(int limit = 20, int offset = 0) {
            var result = await UnitOfWork.GetRepository<User>()
                .Where(u => !u.IsArchive).OrderBy(u => u.Id).Skip(offset).Take(limit).ToListAsync();
            return Mapper.Map<ICollection<UserDto>>(result);
        }

        public async Task<ICollection<UserDto>> GetByRecipientAsync(string recipient, Guid currentUserId) {
            var result = await UnitOfWork.GetRepository<User>()
                .Where(u => u.Id != currentUserId && !u.IsArchive && (u.FirstName + " " + u.LastName).Contains(recipient)).ToListAsync();
            return Mapper.Map<ICollection<UserDto>>(result);
        }

        public async Task<UserDto> GetByIdAsync(Guid id) {
            if (id == Guid.Empty) {
                throw new ArgumentNullException("Id can't be empty.");
            }

            var result = await UnitOfWork.GetRepository<User>().FindByIdAsync(id);
            if (result == null || result.IsArchive) {
                throw new NotFoundException($"User with id = {id} not found.");
            }

            return Mapper.Map<UserDto>(result);
        }
    }
}
