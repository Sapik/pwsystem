﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using PWSystem.Data.Entities;
using PWSystem.DAL;

namespace PWSystem.BLL.Identity {
    public class PWSystemUserManager : UserManager<User, Guid> {
        public PWSystemUserManager(PWSystemUserStore store)
            : base(store) {
        }

        public static PWSystemUserManager Create(IdentityFactoryOptions<PWSystemUserManager> options, IOwinContext context) {
            var manager = new PWSystemUserManager(new PWSystemUserStore(context.Get<PWDbContext>()));
            return manager;
        }
    }
}
