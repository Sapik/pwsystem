﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PWSystem.Data.Entities;
using PWSystem.Data.Identity;

namespace PWSystem.BLL.Identity {
    public class PWSystemUserStore : UserStore<User, Role, Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim> {
        public PWSystemUserStore(DbContext context)
            : base(context) {
        }
    }
}
