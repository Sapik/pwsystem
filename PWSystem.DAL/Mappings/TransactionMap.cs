﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWSystem.Data.Entities;

namespace PWSystem.DAL.Mappings {
    public class TransactionMap : EntityTypeConfiguration<Transaction> {
        public TransactionMap() {
            ToTable("Transactions");

            HasKey(t => t.Id);

            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(t => t.FromUser).WithMany(u => u.FromTransactions).HasForeignKey(t => t.FromUserId).WillCascadeOnDelete(false);
            HasRequired(t => t.ToUser).WithMany(u => u.ToTransactions).HasForeignKey(t => t.ToUserId).WillCascadeOnDelete(false);
        }
    }
}
