﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWSystem.Data.Entities;

namespace PWSystem.DAL.Mappings {
    public class RoleMap : EntityTypeConfiguration<Role> {
        public RoleMap() {
            ToTable("Roles");
        }
    }
}
