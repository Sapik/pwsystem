﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWSystem.Data.Entities;

namespace PWSystem.DAL.Mappings {
    public class UserMap : EntityTypeConfiguration<User> {
        public UserMap() {
            ToTable("Users");

            HasKey(u => u.Id);

            Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasMany(u => u.FromTransactions).WithRequired(t => t.FromUser).HasForeignKey(t => t.FromUserId).WillCascadeOnDelete(false);
            HasMany(u => u.ToTransactions).WithRequired(t => t.ToUser).HasForeignKey(t => t.ToUserId).WillCascadeOnDelete(false);
        }
    }
}
