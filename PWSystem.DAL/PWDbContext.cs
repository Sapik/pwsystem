﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PWSystem.Data.Entities;
using PWSystem.Data.Identity;
using PWSystem.DAL.Mappings;

namespace PWSystem.DAL {
    public class PWDbContext : IdentityDbContext<User, Role, Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim> {
        public PWDbContext() : base("PWDbContext") {
#if DEBUG
            Database.Log = e => Debug.WriteLine(e);
#endif
        }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new TransactionMap());

            modelBuilder.Entity<AspnetUserLogin>().ToTable("AspnetUserLogin");
            modelBuilder.Entity<AspnetUserClaim>().ToTable("AspnetUserClaim");
            modelBuilder.Entity<AspnetUserRole>().ToTable("AspnetUserRole");
        }

        public static PWDbContext Create() {
            return new PWDbContext();
        }
    }
}
